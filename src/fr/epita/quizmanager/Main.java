package fr.epita.quizmanager;

import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.views.MainMenuVD;

/**
 * Main
 * Application entry point. Checks for database path in arguments and stores it in State.
 * Database then loads persistence from file, then the main menu view is rendered.
 */
public class Main {
    private static final String DB_DEFAULT_PATH = "database.json";

    public static void main(String[] args) {
        var persistencePath = DB_DEFAULT_PATH;
        if (args.length > 0) {
            persistencePath = args[0];
        }

        State.getInstance().setDatabasePath(persistencePath);
        Database.setPersistence(Database.persistenceFromFile(persistencePath));
        RenderStack.getInstance().enterView(new MainMenuVD());
    }
}


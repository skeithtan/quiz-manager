package fr.epita.quizmanager.services.state;

import fr.epita.quizmanager.models.Quiz;
import fr.epita.quizmanager.models.QuizEvaluation;

import java.util.ArrayList;
import java.util.List;

/**
 * State
 * The state holds temporary global application state information.
 */
public class State {
    private String databasePath;
    private final static State instance;
    private int selectedQuestionIndex;
    private boolean selectedQuestionHasChoices;
    private int selectedTopicIndex;
    private Quiz currentQuiz;
    private List<QuizEvaluation> evaluations;
    private int selectedEvaluationIndex;

    static {
        instance = new State();
    }

    private State() {
    }

    public static State getInstance() {
        return instance;
    }

    public String getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath = databasePath;
    }

    public int getSelectedQuestionIndex() {
        return selectedQuestionIndex;
    }

    public void setSelectedQuestionIndex(int selectedQuestionIndex) {
        this.selectedQuestionIndex = selectedQuestionIndex;
    }

    public boolean selectedQuestionHasChoices() {
        return selectedQuestionHasChoices;
    }

    public void setSelectedQuestion(int index, boolean hasChoices) {
        this.selectedQuestionIndex = index;
        this.selectedQuestionHasChoices = hasChoices;
    }

    public void setSelectedQuestionHasChoices(boolean selectedQuestionHasChoices) {
        this.selectedQuestionHasChoices = selectedQuestionHasChoices;
    }

    public int getSelectedTopicIndex() {
        return selectedTopicIndex;
    }

    public void setSelectedTopicIndex(int selectedTopicIndex) {
        this.selectedTopicIndex = selectedTopicIndex;
    }

    public Quiz getCurrentQuiz() {
        return currentQuiz;
    }

    public void setCurrentQuiz(Quiz currentQuiz) {
        this.currentQuiz = currentQuiz;
        evaluations = new ArrayList<>();
    }

    public QuizEvaluation[] getEvaluations() {
        return evaluations.toArray(QuizEvaluation[]::new);
    }

    public int getSelectedEvaluationIndex() {
        return selectedEvaluationIndex;
    }

    public QuizEvaluation getSelectedEvaluation() {
        return evaluations.get(this.selectedEvaluationIndex);
    }

    public void setSelectedEvaluationIndex(int selectedEvaluation) {
        this.selectedEvaluationIndex = selectedEvaluation;
    }

    public void addEvaluation(QuizEvaluation evaluation) {
        this.evaluations.add(evaluation);
    }

    public void addEvaluationAndSetCurrent(QuizEvaluation evaluation) {
        addEvaluation(evaluation);
        setSelectedEvaluationIndex(evaluations.size() - 1);
    }

    public void removeCurrentEvaluation() {
        this.evaluations.remove(this.selectedEvaluationIndex);
        this.selectedEvaluationIndex = 0;
    }
}

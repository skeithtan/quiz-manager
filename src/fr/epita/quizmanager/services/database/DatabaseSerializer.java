package fr.epita.quizmanager.services.database;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * DatabaseSerializer
 * The database serializer takes the InMemoryPersistence and stores the data into a JSON object.
 */
public class DatabaseSerializer {
    private final InMemoryPersistence database;

    public DatabaseSerializer(InMemoryPersistence database) {
        this.database = database;
    }

    public void saveToFile(String path) throws IOException {
        var rootObject = databaseToJSON(database);

        var file = new FileWriter(path);
        file.write(JSONObject.toJSONString(rootObject));
        file.flush();
        file.close();
    }

    private Map<String, Object> databaseToJSON(InMemoryPersistence database) {
        var rootObject = new HashMap<String, Object>();
        rootObject.put(SerializationKey.TOPICS.getKey(), Arrays.asList(database.getTopics()));
        rootObject.put(SerializationKey.CHOICE_QUESTIONS.getKey(), questionsToJSON(database.getChoiceQuestions()));
        rootObject.put(SerializationKey.OPEN_QUESTIONS.getKey(), questionsToJSON(database.getOpenQuestions()));

        return rootObject;
    }

    private List<Map<String, Object>> questionsToJSON(Question[] questions) {
        return Arrays.stream(questions)
                .map(question -> question instanceof ChoiceQuestion ?
                        choiceQuestionToJSON((ChoiceQuestion) question) :
                        questionToJSON(question)
                )
                .collect(Collectors.toList());
    }

    private Map<String, Object> choiceQuestionToJSON(ChoiceQuestion choiceQuestion) {
        var questionObject = (Map<String, Object>) questionToJSON(choiceQuestion);
        questionObject.put(SerializationKey.QUESTION_CHOICES.getKey(), choiceQuestion.getChoices());
        questionObject.put(SerializationKey.QUESTION_CORRECT_ANSWER.getKey(), choiceQuestion.getCorrectAnswer());

        return questionObject;
    }

    private Map<String, Object> questionToJSON(Question question) {
        var questionObject = new HashMap<String, Object>();
        questionObject.put(SerializationKey.QUESTION_LABEL.getKey(), question.getLabel());
        questionObject.put(SerializationKey.QUESTION_TOPICS.getKey(), question.getTopics());
        questionObject.put(SerializationKey.QUESTION_DIFFICULTY_LEVEL.getKey(), question.getDifficultyLevel());

        return questionObject;
    }

}

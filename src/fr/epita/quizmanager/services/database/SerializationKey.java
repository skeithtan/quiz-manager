package fr.epita.quizmanager.services.database;

/**
 * SerializationKey
 * The serialization key contains standardized JSON keys for object fields.
 */
public enum SerializationKey {
    TOPICS("topics"),
    OPEN_QUESTIONS("open_questions"),
    CHOICE_QUESTIONS("choice_questions"),
    QUESTION_LABEL("label"),
    QUESTION_TOPICS("topics"),
    QUESTION_DIFFICULTY_LEVEL("difficulty_level"),
    QUESTION_CHOICES("choices"),
    QUESTION_CORRECT_ANSWER("correct_answer");

    private final String key;

    SerializationKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }
}

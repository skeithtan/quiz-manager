package fr.epita.quizmanager.services.database;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

/**
 * DatabaseDeserializer
 * The database deserializer reads a JSON object created by the DatabaseSerializer
 * and loads the data into an InMemoryPersistence instance
 */
@SuppressWarnings("unchecked")
public class DatabaseDeserializer {

    public InMemoryPersistence loadFromFile(String path) {
        ArrayList<Question> questions;
        ArrayList<ChoiceQuestion> choiceQuestions;
        ArrayList<String> topics;

        try {
            var fileContents = new String(Files.readAllBytes(Paths.get(path)));
            var jsonObject = (JSONObject) new JSONParser().parse(fileContents);
            var topicsJA = (JSONArray) jsonObject.get(SerializationKey.TOPICS.getKey());
            var openQuestionsJA = (JSONArray) jsonObject.get(SerializationKey.OPEN_QUESTIONS.getKey());
            var choiceQuestionsJA = (JSONArray) jsonObject.get(SerializationKey.CHOICE_QUESTIONS.getKey());

            topics = topicsJA;
            questions = parseQuestions(openQuestionsJA);
            choiceQuestions = parseChoiceQuestions(choiceQuestionsJA);
        } catch (Exception e) {

            if (e instanceof NoSuchFileException) {
                System.err.printf("File '%s' could not be found\n", path);
            } else {
                System.err.printf("Unknown error occurred: %s", e.getMessage());
            }

            System.err.println("Initializing with blank memory...");

            topics = new ArrayList<>();
            questions = new ArrayList<>();
            choiceQuestions = new ArrayList<>();
        }

        System.out.println("Successfully loaded database.");
        return new InMemoryPersistence(topics, questions, choiceQuestions);
    }

    private ArrayList<Question> parseQuestions(JSONArray questionsJA) {
        var questions = new ArrayList<Question>();
        for (var o : questionsJA) {
            var questionMap = (Map<String, Object>) o;
            questions.add(parseQuestion(questionMap));
        }
        return questions;
    }

    private ArrayList<ChoiceQuestion> parseChoiceQuestions(JSONArray choiceQuestionsJA) throws Exception {
        var choiceQuestions = new ArrayList<ChoiceQuestion>();
        for (var o : choiceQuestionsJA) {
            var choiceQuestionMap = (Map<String, Object>) o;
            choiceQuestions.add(parseChoiceQuestion(choiceQuestionMap));
        }
        return choiceQuestions;
    }

    private Question parseQuestion(Map<String, Object> questionMap) {
        var topicsJA = (JSONArray) questionMap.get(SerializationKey.QUESTION_TOPICS.getKey());
        var topics = new ArrayList<String>();

        for (var o : topicsJA) {
            var topicName = (String) o;
            topics.add(topicName);
        }

        var label = (String) questionMap.get(SerializationKey.QUESTION_LABEL.getKey());
        var difficultyLevel = (Long) questionMap.get(SerializationKey.QUESTION_DIFFICULTY_LEVEL.getKey());

        return new Question(label, topics, difficultyLevel.intValue());
    }

    private ChoiceQuestion parseChoiceQuestion(Map<String, Object> choiceQuestionMap) {
        var choiceQuestionsJA = (JSONArray) choiceQuestionMap.get(SerializationKey.QUESTION_CHOICES.getKey());
        var correctAnswer = choiceQuestionMap.get(SerializationKey.QUESTION_CORRECT_ANSWER.getKey()).toString();
        var choiceItems = new ArrayList<String>();
        for (Object o : choiceQuestionsJA) {
            choiceItems.add(o.toString());
        }

        var q = parseQuestion(choiceQuestionMap);
        return new ChoiceQuestion(q, choiceItems, correctAnswer);
    }
}

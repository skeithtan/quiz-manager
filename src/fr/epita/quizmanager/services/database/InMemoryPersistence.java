package fr.epita.quizmanager.services.database;

import fr.epita.quizmanager.Main;
import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.services.state.State;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * InMemoryPersistence
 * The InMemoryPersistence is a cache containing serializable objects.
 */
public class InMemoryPersistence {
    private final ArrayList<String> topics;
    private final ArrayList<Question> openQuestions;
    private final ArrayList<ChoiceQuestion> choiceQuestions;

    public InMemoryPersistence(ArrayList<String> topics, ArrayList<Question> openQuestions, ArrayList<ChoiceQuestion> choiceQuestions) {
        this.topics = topics;
        this.openQuestions = openQuestions;
        this.choiceQuestions = choiceQuestions;
    }

    public String[] getTopics() {
        return topics.toArray(String[]::new);
    }

    public String getTopic(int index) {
        return topics.get(index);
    }

    public Question[] getOpenQuestions() {
        return openQuestions.toArray(Question[]::new);
    }

    public ChoiceQuestion[] getChoiceQuestions() {
        return choiceQuestions.toArray(ChoiceQuestion[]::new);
    }

    public Question[] getAllQuestions() {
        return Stream
                .concat(openQuestions.stream(), choiceQuestions.stream())
                .toArray(Question[]::new);
    }

    public ChoiceQuestion getChoiceQuestion(int index) {
        return choiceQuestions.get(index);
    }

    public Question getOpenQuestion(int index) {
        return openQuestions.get(index);
    }

    public void addTopic(String topic) {
        this.topics.add(topic);
        persist();
    }

    public int addOpenQuestion(Question openQuestion) {
        this.openQuestions.add(openQuestion);
        persist();
        return this.openQuestions.size() - 1;
    }

    public int addChoiceQuestion(ChoiceQuestion choiceQuestion) {
        this.choiceQuestions.add(choiceQuestion);
        persist();
        return this.choiceQuestions.size() - 1;
    }

    public void updateTopic(int index, String replacement) {
        var oldTopic = topics.get(index);
        topics.set(index, replacement);

        Stream.concat(openQuestions.stream(), choiceQuestions.stream())
                .forEach(question -> {
                    question.getTopics().remove(oldTopic);
                    question.getTopics().add(replacement);
                });

        persist();
    }

    private void updateOpenQuestion(int index, Question replacement) {
        openQuestions.set(index, replacement);
        persist();
    }

    private void updateChoiceQuestion(int index, ChoiceQuestion replacement) {
        choiceQuestions.set(index, replacement);
        persist();
    }

    public void updateQuestion(int index, Question replacement) {
        if (replacement instanceof ChoiceQuestion) {
            updateChoiceQuestion(index, (ChoiceQuestion) replacement);
        } else {
            updateOpenQuestion(index, replacement);
        }
        persist();
    }

    public void removeOpenQuestion(Question openQuestion) {
        this.openQuestions.remove(openQuestion);
        persist();
    }

    public void removeChoiceQuestion(ChoiceQuestion choiceQuestion) {
        this.choiceQuestions.remove(choiceQuestion);
        persist();
    }

    public void removeOpenQuestion(int index) {
        this.openQuestions.remove(index);
        persist();
    }

    public void removeChoiceQuestion(int index) {
        this.choiceQuestions.remove(index);
        persist();
    }

    private void persist() {
        try {
            var persistencePath = State.getInstance().getDatabasePath();
            new DatabaseSerializer(Database.getPersistence()).saveToFile(persistencePath);
        } catch (IOException e) {
            System.err.println("An error occurred saving to file");
            e.printStackTrace();
        }
    }
}

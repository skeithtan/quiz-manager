package fr.epita.quizmanager.services.database;

/**
 * Database
 * The database class holds the global persistence object
 */
public class Database {
    private static InMemoryPersistence persistence;

    public static InMemoryPersistence getPersistence() {
        return Database.persistence;
    }

    public static void setPersistence(InMemoryPersistence persistence) {
        Database.persistence = persistence;
    }

    public static InMemoryPersistence persistenceFromFile(String path) {
        return new DatabaseDeserializer().loadFromFile(path);
    }
}

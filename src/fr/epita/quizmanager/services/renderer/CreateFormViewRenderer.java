package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.CreateFormViewDelegate;

public class CreateFormViewRenderer extends FormViewRenderer {
    private final CreateFormViewDelegate delegate;

    public CreateFormViewRenderer(CreateFormViewDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public CreateFormViewDelegate getDelegate() {
        return delegate;
    }
}


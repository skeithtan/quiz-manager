package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.ViewDelegate;

/**
 * ViewRenderer
 * A view renderer takes data from a given view delegate and formats it to the screen.
 * The view renderer also takes user input and feeds it back to the delegate.
 * @param <V> ViewDelegate implementation
 */
public interface ViewRenderer<V extends ViewDelegate> {
    void display();

    V getDelegate();

    default String getViewTitle() {
        return getDelegate().getViewTitle();
    }

    default void exitView() {
        getDelegate().onExitView();
    }
}

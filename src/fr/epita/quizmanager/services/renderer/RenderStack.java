package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.*;

import java.util.Stack;

/**
 * RenderStack
 * The render stack is a centralized handler for ViewRenderer instances.
 * The RenderStack handles navigation, entering into view,
 * exiting into parent view, and making navigation breadcrumbs.
 */
public class RenderStack {
    private final Stack<ViewRenderer<?>> viewStack;
    private static final RenderStack instance;

    static {
        instance = new RenderStack();
    }

    private RenderStack() {
        viewStack = new Stack<>();
    }

    public static RenderStack getInstance() {
        return instance;
    }

    public void enterView(ViewRenderer<?> view) {
        viewStack.push(view);
        renderTopView();
    }

    public void enterView(MenuViewDelegate delegate) {
        enterView(new MenuViewRenderer(delegate));
    }

    public void enterView(SectionedMenuViewDelegate delegate) {
        enterView(new SectionedMenuViewRenderer(delegate));
    }

    public void enterView(CreateFormViewDelegate delegate) {
        enterView(new CreateFormViewRenderer(delegate));
    }

    public void enterView(UpdateFormViewDelegate delegate) {
        enterView(new UpdateFormViewRenderer(delegate));
    }

    public void enterView(ConfirmDialogViewDelegate delegate) {
        enterView(new ConfirmDialogViewRenderer(delegate));
    }

    public void enterView(SimplePromptViewDelegate delegate) {
        enterView(new SimplePromptViewRenderer(delegate));
    }

    public void enterView(MultipleEntryFormDelegate delegate) {
        enterView(new MultipleEntryViewRenderer(delegate));
    }

    public void exitToParentAndRenderTop(ViewRenderer<?> view) {
        while (viewStack.peek() != view) {
            viewStack.pop();
        }

        // Now that the top view is the actual view, pop again to go to parent
        viewStack.pop();

        renderTopView();
    }

    public void exitToParent(ViewDelegate delegate) {
        while (viewStack.peek().getDelegate() != delegate) {
            viewStack.pop();
        }

        viewStack.pop();
    }

    public void exitToParentAndRenderTop(ViewDelegate delegate) {
        exitToParent(delegate);
        renderTopView();
    }

    private String makeNavigationTitle() {
        var topIdx = viewStack.size() - 1;
        var bottomIdx = topIdx - 2;

        // Prevent out of bounds
        if (bottomIdx < 0) {
            bottomIdx = 0;
        }

        if (topIdx == 0) {
            return viewStack.peek().getViewTitle();
        }

        var navigationTitle = "";

        for (int i = topIdx; i >= bottomIdx; i--) {
            var view = viewStack.get(i);
            navigationTitle = navigationTitle + " " + view.getViewTitle();
            navigationTitle = i == bottomIdx ? navigationTitle : navigationTitle + " < ";
        }

        var shouldClip = bottomIdx > 0;
        navigationTitle = shouldClip ? navigationTitle + " < ..." : navigationTitle;

        return navigationTitle.trim();
    }

    private void renderView(ViewRenderer<?> view) {
        System.out.println();
        System.out.println(this.makeNavigationTitle());

        var viewText = view.getDelegate().getViewText();

        if (viewText != null) {
            System.out.println(viewText);
        }

        view.display();
    }

    private void renderTopView() {
        if (this.viewStack.empty()) {
            System.exit(0);
        }

        renderView(this.viewStack.peek());
    }

}

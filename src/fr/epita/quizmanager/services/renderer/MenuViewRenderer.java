package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.MenuViewDelegate;

import java.util.Scanner;

public class MenuViewRenderer implements ViewRenderer<MenuViewDelegate> {
    private static final Scanner scanner = new Scanner(System.in);
    private final MenuViewDelegate delegate;

    public MenuViewRenderer(MenuViewDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public MenuViewDelegate getDelegate() {
        return delegate;
    }

    private void onOptionSelect(int optionNumber) {
        if (optionNumber < 0 || optionNumber > delegate.getNumberOfOptions()) {
            this.onInvalidOptionSelect(Integer.toString(optionNumber));
            return;
        }

        if (optionNumber == 0) {
            this.exitView();
            return;
        }

        this.delegate.onSelectOption(optionNumber);
    }

    private void onInvalidOptionSelect(String userInput) {
        System.out.printf("Invalid option '%s' selected. Try again.\n", userInput);
        this.promptUser();
    }

    private void promptUser() {
        System.out.printf("%s > ", delegate.getPromptLabel());
        var input = scanner.nextLine();
        try {
            onOptionSelect(Integer.parseInt(input));
        } catch (NumberFormatException e) {
            this.onInvalidOptionSelect(input);
        }
    }

    public void display() {
        for (int i = 1; i <= delegate.getNumberOfOptions(); i++) {
            var label = delegate.getLabelForOption(i);
            System.out.printf("\t[%d] - %s\n", i, label);
        }

        System.out.println("\t-------");
        System.out.printf("\t[0] - %s\n", delegate.getExitMenuLabel());

        this.promptUser();
    }

}

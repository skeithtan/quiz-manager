package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.MultipleEntryFormDelegate;

import java.util.Scanner;

public class MultipleEntryViewRenderer implements ViewRenderer<MultipleEntryFormDelegate> {
    private final MultipleEntryFormDelegate delegate;
    private static final Scanner scanner = new Scanner(System.in);

    public MultipleEntryViewRenderer(MultipleEntryFormDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public MultipleEntryFormDelegate getDelegate() {
        return delegate;
    }

    @Override
    public void display() {
        for (int i = 1; i <= delegate.getNumberOfEntries(); i++) {
            getEntry(i);
        }
    }

    private void getEntry(int entryNumber) {
        System.out.printf("%s > ", delegate.getPromptLabel(entryNumber));
        var entry = scanner.nextLine();
        var validationResult = delegate.validateEntry(entry);

        if (validationResult.isNotValid()) {
            System.out.println(validationResult.getValidationError());
            getEntry(entryNumber);
        } else {
            delegate.onValidEntry(entry, entryNumber);
        }
    }
}

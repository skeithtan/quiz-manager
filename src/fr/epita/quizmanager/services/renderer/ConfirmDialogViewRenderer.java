package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.ConfirmDialogViewDelegate;

import java.util.Scanner;

public class ConfirmDialogViewRenderer implements ViewRenderer<ConfirmDialogViewDelegate> {
    private static final Scanner scanner = new Scanner(System.in);
    private final ConfirmDialogViewDelegate delegate;

    public ConfirmDialogViewRenderer(ConfirmDialogViewDelegate delegate) {
        this.delegate = delegate;
    }

    public void display() {
        promptUser();
    }

    @Override
    public ConfirmDialogViewDelegate getDelegate() {
        return delegate;
    }

    public void promptUser() {
        System.out.printf("\t[1] - %s\n", delegate.getPositiveLabel());
        System.out.printf("\t[0] - %s\n", delegate.getNegativeLabel());

        String input;

        do {
            System.out.print("Select an option > ");
            input = scanner.nextLine().trim();
        } while (!(input.equals("1") || input.equals("0")));

        var isPositive = input.equals("1");

        if (isPositive) {
            delegate.onSelectPositive();
        } else {
            delegate.onSelectNegative();
        }

        delegate.onExitView();
        ViewRenderer.super.exitView();
    }
}

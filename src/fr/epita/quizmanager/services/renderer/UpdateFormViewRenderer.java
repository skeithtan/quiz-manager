package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.view.UpdateFormViewDelegate;

public class UpdateFormViewRenderer extends FormViewRenderer {
    private final UpdateFormViewDelegate delegate;

    public UpdateFormViewRenderer(UpdateFormViewDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public UpdateFormViewDelegate getDelegate() {
        return delegate;
    }

    @Override
    protected void promptForField(int fieldNumber) {
        var delegate = this.getDelegate();
        var prompt = delegate.getFieldName(fieldNumber);
        var oldValue = delegate.getFieldOldValue(fieldNumber);
        var instructions = delegate.getFieldEntryInstructions(fieldNumber);

        String input;
        ValidationResult validation;

        do {
            if (instructions != null) {
                System.out.println("\n" + instructions);
            }

            System.out.println("Press enter without entering a value to retain old value");
            System.out.printf("Old value: '%s'\n", oldValue);
            System.out.print(prompt + " > ");
            input = scanner.nextLine().trim();

            if (input.isEmpty()) {
                delegate.onPreserveValue(fieldNumber);
                return;
            }

            validation = delegate.validateValue(fieldNumber, input);

            if (validation.isNotValid()) {
                System.out.println(validation.getValidationError());
            }
        } while (validation.isNotValid());

        delegate.onEnterValidValue(fieldNumber, input);
    }

    @Override
    protected String getFieldSummary(int fieldNumber) {
        var delegate = this.getDelegate();
        var fieldName = delegate.getFieldName(fieldNumber);
        var oldValue = delegate.getFieldOldValue(fieldNumber);
        var newValue = delegate.getFieldValue(fieldNumber);

        if (oldValue.equals(newValue)) {
            return String.format("%s: %s", fieldName, newValue);
        } else {
            return String.format(
                    "%s:\n\t\tOld value: '%s'\n\t\tNew value: '%s'",
                    fieldName,
                    delegate.getFieldOldValue(fieldNumber),
                    delegate.getFieldValue(fieldNumber)
            );
        }
    }

    @Override
    public void display() {
        // With one field, directly modify this one field
        if (delegate.getNumberOfFields() == 1) {
            super.promptUser();
        } else {
            super.showUpdateValueMenu();
        }

        super.showSummary();
    }
}

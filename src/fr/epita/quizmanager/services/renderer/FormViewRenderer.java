package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.view.FormViewDelegate;
import fr.epita.quizmanager.view.MenuViewDelegate;

import java.util.Scanner;

public abstract class FormViewRenderer implements ViewRenderer<FormViewDelegate> {
    protected static final Scanner scanner = new Scanner(System.in);

    @Override
    public void display() {
        promptUser();
        showSummary();
    }

    protected void promptUser() {
        var fieldCount = this.getDelegate().getNumberOfFields();
        for (int i = 1; i <= fieldCount; i++) {
            promptForField(i);
        }
    }

    protected void promptForField(int fieldNumber) {
        var delegate = this.getDelegate();
        var prompt = delegate.getFieldName(fieldNumber);
        var instructions = delegate.getFieldEntryInstructions(fieldNumber);

        String input;
        ValidationResult validation;

        do {
            if (instructions != null) {
                System.out.println("\n" + instructions);
            }

            System.out.printf("%s > ", prompt);
            input = scanner.nextLine().trim();
            validation = delegate.validateValue(fieldNumber, input);

            if (validation.isNotValid()) {
                System.out.println(validation.getValidationError());
            }
        } while (validation.isNotValid());

        delegate.onEnterValidValue(fieldNumber, input);
    }

    protected void showSummary() {
        System.out.println("Summary");
        for (int i = 1; i <= this.getDelegate().getNumberOfFields(); i++) {
            System.out.println("\t" + getFieldSummary(i));
        }

        RenderStack.getInstance().enterView(getConfirmDialogDelegate());
    }

    protected String getFieldSummary(int fieldNumber) {
        return String.format(
                "%s: %s",
                this.getDelegate().getFieldName(fieldNumber),
                this.getDelegate().getFieldValue(fieldNumber)
        );
    }

    protected void showUpdateValueMenu() {
        RenderStack.getInstance().enterView(getUpdateValueMenuDelegate());
        showSummary();
    }

    protected MenuViewDelegate getConfirmDialogDelegate() {
        return new MenuViewDelegate() {
            @Override
            public int getNumberOfOptions() {
                return 2;
            }

            @Override
            public String getLabelForOption(int optionNumber) {
                switch (optionNumber) {
                    case 1:
                        return "Confirm and save";
                    case 2:
                        return "Modify a value";
                    default:
                        return null;
                }
            }

            @Override
            public void onSelectOption(int optionNumber) {
                switch (optionNumber) {
                    case 1:
                        onConfirmSummary();
                        return;
                    case 2:
                        showUpdateValueMenu();
                }
            }

            @Override
            public String getExitMenuLabel() {
                return "Cancel and discard changes";
            }

            @Override
            public String getViewTitle() {
                return "Confirm summary";
            }

            public void onConfirmSummary() {
                FormViewRenderer.this.getDelegate().onSubmitForm();
                System.out.println(FormViewRenderer.this.getDelegate().getFormReceipt());
                this.onExitView();
            }

            @Override
            public void onExitView() {
                FormViewRenderer.this.exitView();
            }
        };
    }

    protected MenuViewDelegate getUpdateValueMenuDelegate() {
        return new MenuViewDelegate() {
            @Override
            public int getNumberOfOptions() {
                return FormViewRenderer.this.getDelegate().getNumberOfFields();
            }

            @Override
            public String getLabelForOption(int optionNumber) {
                return getFieldSummary(optionNumber);
            }

            @Override
            public void onSelectOption(int optionNumber) {
                promptForField(optionNumber);
            }

            @Override
            public String getViewTitle() {
                return "Select field to modify";
            }

            @Override
            public void onExitView() {
                FormViewRenderer.this.exitView();
            }
        };
    }
}

package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.SectionedMenuViewDelegate;

import java.util.Scanner;

public class SectionedMenuViewRenderer implements ViewRenderer<SectionedMenuViewDelegate> {
    private static final Scanner scanner = new Scanner(System.in);
    private final SectionedMenuViewDelegate delegate;

    public SectionedMenuViewRenderer(SectionedMenuViewDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void display() {
        for (int i = 1; i <= delegate.getNumberOfSections(); i++) {
            System.out.printf("\t%s\n", delegate.getLabelForSection(i));
            var optionCount = delegate.getNumberOfOptionsForSection(i);

            if (optionCount == 0) {
                System.out.println("\t\tNothing in this section");
            }

            for (int j = 1; j <= optionCount; j++) {
                var label = delegate.getLabelForOption(i, j);
                System.out.printf("\t\t[%d-%d] - %s\n", i, j, label);
            }

            System.out.println();
        }
        System.out.println("\t-------");
        System.out.printf("\t[0] - %s\n", delegate.getExitMenuLabel());
        this.promptUser();
    }

    @Override
    public SectionedMenuViewDelegate getDelegate() {
        return delegate;
    }

    private void promptUser() {
        System.out.printf("%s > ", delegate.getPromptLabel());
        var input = scanner.nextLine();
        onUserInput(input.trim());
    }

    private void onUserInput(String userInput) {
        if (userInput.equals("0")) {
            this.exitView();
            return;
        }

        var splitInput = userInput.split("-");

        if (splitInput.length != 2) {
            this.onInvalidOptionSelect(userInput);
            return;
        }

        int selectedOption;
        int selectedSection;

        try {
            selectedSection = Integer.parseInt(splitInput[0]);
            selectedOption = Integer.parseInt(splitInput[1]);
        } catch (NumberFormatException e) {
            this.onInvalidOptionSelect(userInput);
            return;
        }

        if (selectedOption < 0 || selectedSection < 0 ||
                selectedSection > delegate.getNumberOfSections() ||
                selectedOption > delegate.getNumberOfOptionsForSection(selectedSection)) {
            this.onInvalidOptionSelect(userInput);
            return;
        }

        this.delegate.onSelectOption(selectedSection, selectedOption);
    }

    private void onInvalidOptionSelect(String userInput) {
        System.out.printf("Invalid option '%s' selected. Try again.\n", userInput);
        this.promptUser();
    }


}

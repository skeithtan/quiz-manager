package fr.epita.quizmanager.services.renderer;

import fr.epita.quizmanager.view.SimplePromptViewDelegate;

import java.util.Scanner;

public class SimplePromptViewRenderer implements ViewRenderer<SimplePromptViewDelegate> {
    private final SimplePromptViewDelegate delegate;
    private static final Scanner scanner = new Scanner(System.in);

    public SimplePromptViewRenderer(SimplePromptViewDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public SimplePromptViewDelegate getDelegate() {
        return delegate;
    }

    @Override
    public void display() {
        promptUser();
    }

    private void promptUser() {
        System.out.printf("%s > ", delegate.getPromptLabel());
        var response = scanner.nextLine();
        var validationResult = delegate.validateResponse(response);

        if (validationResult.isNotValid()) {
            System.out.println(validationResult.getValidationError());
            promptUser();
        } else {
            delegate.onEnterValidResponse(response);
        }
    }

}

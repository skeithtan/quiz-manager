package fr.epita.quizmanager.services.quiz;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.models.Quiz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * QuizSerializer
 * The quiz serializer formats a quiz and outputs it into a text file.
 */
public class QuizSerializer {
    private final Quiz quiz;
    private static final int OPEN_QUESTIONS_BLANK_LINE_COUNT = 3;
    private static final int OPEN_QUESTIONS_UNDERSCORE_COUNT = 50;

    public QuizSerializer(Quiz quiz) {
        this.quiz = quiz;
    }

    public void makeFile(String fileName) {
        try {
            var file = new File(fileName);
            var writer = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
            var contents = makeQuizString();

            writer.write(contents);
            writer.close();
        } catch (Exception e) {
            System.err.println("An error occurred saving to file");
            e.printStackTrace();
        }

    }

    private String makeQuizString() {
        StringBuilder output = new StringBuilder()
                .append(quiz.getTitle()).append("\n")
                .append(getTopicsHeading()).append("\n")
                .append(getDifficultyHeading()).append("\n")
                .append(getNumberOfQuestionsHeading()).append("\n")
                .append("\n\n"); // Extra padding before questions start

        var questions = quiz.getQuestions();
        for (int i = 0; i < questions.size(); i++) {
            var questionNumber = i + 1;
            var question = questions.get(i);
            output.append(questionToString(question, questionNumber))
                    .append("\n");
        }

        output.append("\nEnd of quiz");
        return output.toString();
    }

    private String getTopicsHeading() {
        return String.format("Topics: %s", String.join(", ", quiz.getTopics()));
    }

    private String getDifficultyHeading() {
        return String.format("Quiz difficulty: %s / 5", quiz.getAverageDifficulty());
    }

    private String getNumberOfQuestionsHeading() {
        return String.format("This quiz contains %d questions", quiz.getQuestions().size());
    }

    private String questionToString(Question question, int questionNumber) {
        var output = new StringBuilder()
                .append(String.format("%d.) %s\n", questionNumber, question.getLabel()));

        if (question instanceof ChoiceQuestion) {
            output.append(choicesToString((ChoiceQuestion) question));
        } else {
            output.append(makeOpenQuestionForm());
        }

        return output.toString();
    }

    private String choicesToString(ChoiceQuestion question) {
        return question.choicesToString();
    }

    private String makeOpenQuestionForm() {
        var blankLines = new StringBuilder();

        for (int i = 1; i <= OPEN_QUESTIONS_BLANK_LINE_COUNT; i++) {
            blankLines.append("_".repeat(OPEN_QUESTIONS_UNDERSCORE_COUNT));
            blankLines.append("\n");
        }

        return blankLines.toString();
    }
}

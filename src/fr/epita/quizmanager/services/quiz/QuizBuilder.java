package fr.epita.quizmanager.services.quiz;

import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.models.Quiz;
import fr.epita.quizmanager.services.database.Database;

import java.util.*;
import java.util.stream.Collectors;

/**
 * QuizBuilder
 * The quiz builder uses the Builder design pattern to construct a Quiz object.
 * It also contains the algorithm for creating a question set with an average
 * difficulty that is as close as possible to the target difficulty.
 */
public class QuizBuilder {
    private int targetQuestionsCount;
    private List<Question> allTopicQuestions;
    private Map<String, List<Question>> questionsByTopic;
    private final Quiz quiz;

    public QuizBuilder() {
        this.quiz = new Quiz();
    }

    public QuizBuilder setTargetDifficulty(int targetDifficulty) {
        this.quiz.setTargetDifficulty(targetDifficulty);
        return this;
    }

    public QuizBuilder setTopics(List<String> topics) {
        this.quiz.setTopics(topics);
        return this;
    }

    public QuizBuilder setTargetQuestionsCount(int targetQuestionsCount) {
        this.targetQuestionsCount = targetQuestionsCount;
        return this;
    }

    public QuizBuilder setTitle(String title) {
        this.quiz.setTitle(title);
        return this;
    }

    public Quiz build() {
        this.allTopicQuestions = findAllTopicQuestions();
        this.questionsByTopic = makeTopicQuestionMap();
        this.quiz.setQuestions(arrangeQuizQuestions());
        return quiz;
    }

    public String getTitle() {
        return this.quiz.getTitle();
    }

    public int getTargetDifficulty() {
        return this.quiz.getTargetDifficulty();
    }

    public int getTargetQuestionsCount() {
        return this.targetQuestionsCount;
    }

    public List<String> getTopics() {
        return this.quiz.getTopics();
    }

    /**
     * Get all questions with desired topics from the database and store in state
     */
    public List<Question> findAllTopicQuestions() {
        return Arrays
                .stream(Database.getPersistence().getAllQuestions())
                .filter(question -> !Collections.disjoint(question.getTopics(), quiz.getTopics()))
                .collect(Collectors.toList());
    }

    /**
     * Sort all questions into appropriate topics
     */
    private Map<String, List<Question>> makeTopicQuestionMap() {
        var topicQuestionMap = new HashMap<String, List<Question>>();

        for (var topic : quiz.getTopics()) {
            var topicQuestions = allTopicQuestions
                    .stream()
                    .filter(question -> question.getTopics().contains(topic))
                    .collect(Collectors.toList());

            topicQuestionMap.put(topic, topicQuestions);
        }

        return topicQuestionMap;
    }

    private List<Question> arrangeQuizQuestions() {
        var quizQuestions = new ArrayList<Question>();

        // In the event that not enough questions are in the database to make a quiz, reduce to realistic quiz length
        if (allTopicQuestions.size() < targetQuestionsCount) {
            this.targetQuestionsCount = allTopicQuestions.size();
            return allTopicQuestions;
        }

        var searchDifficulty = quiz.getTargetDifficulty();
        var topicsIterator = quiz.getTopics().iterator();

        while (quizQuestions.size() < this.targetQuestionsCount) {
            var pool = questionsByTopic.get(topicsIterator.next());
            if (pool.isEmpty()) {
                continue;
            }

            // Find a question, add to quiz, and remove from search pools
            var index = findIndexOfClosestDifficulty(pool, searchDifficulty);
            var question = pool.get(index);
            quizQuestions.add(question);
            removeFromMap(question);

            // Adjust search difficulty to meet desired average
            var offset = (int) Math.ceil(quiz.getTargetDifficulty() - getAverageDifficulty(quizQuestions));
            searchDifficulty = quiz.getTargetDifficulty() + offset;

            // Loop again to start when iterator is empty
            if (!topicsIterator.hasNext()) {
                topicsIterator = quiz.getTopics().iterator();
            }
        }

        return quizQuestions;
    }

    private double getAverageDifficulty(List<Question> questions) {
        return questions.stream()
                .mapToDouble(Question::getDifficultyLevel)
                .average()
                .orElse(0);
    }

    private void removeFromMap(Question question) {
        for (var entry : questionsByTopic.entrySet()) {
            if (!entry.getValue().contains(question)) {
                continue;
            }

            entry.getValue().remove(question);
        }
    }

    /**
     * Find index of closest difficulty
     * Searches in a given pool the index of a question with the exact difficulty level. If not found,
     * the offset is incremented and the difficulty level search is expanded.
     *
     * @param pool             Pool of questions
     * @param targetDifficulty Target difficulty
     * @return Index of closest difficulty
     */
    private static int findIndexOfClosestDifficulty(List<Question> pool, int targetDifficulty) {
        var index = findIndexWithDifficulty(pool, targetDifficulty);
        if (index != -1) {
            return index;
        }

        var offset = 1;
        while (true) {
            if (targetDifficulty - offset >= Question.MIN_DIFFICULTY) {
                index = findIndexWithDifficulty(pool, targetDifficulty - offset);
            }

            if (index != -1) {
                return index;
            }

            if (targetDifficulty + offset <= Question.MAX_DIFFICULTY) {
                index = findIndexWithDifficulty(pool, targetDifficulty + offset);
            }

            if (index != -1) {
                return index;
            }

            offset++;
        }
    }

    /**
     * Find index with difficulty:
     * Searches in a given pool the index of a question with the exact difficulty level. If not found, return -1.
     *
     * @param pool             Pool of questions
     * @param targetDifficulty Target difficulty
     * @return Index of question with exact difficulty or -1
     */
    private static int findIndexWithDifficulty(List<Question> pool, int targetDifficulty) {
        for (int i = 0; i < pool.size(); i++) {
            if (pool.get(i).getDifficultyLevel() == targetDifficulty) {
                return i;
            }
        }

        return -1;
    }

}

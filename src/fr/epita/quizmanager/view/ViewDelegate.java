package fr.epita.quizmanager.view;

import fr.epita.quizmanager.services.renderer.RenderStack;

/**
 * ViewDelegate
 * The view delegate supplies data to be displayed and input handling logic.
 * The delegate must never directly display any data - this is the renderer's job.
 */
public interface ViewDelegate {
    String getViewTitle();

    default String getViewText() {
        return null;
    }

    default void onExitView() {
        RenderStack.getInstance().exitToParentAndRenderTop(this);
    }
}

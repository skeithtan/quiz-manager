package fr.epita.quizmanager.view;

/**
 * SectionedMenuViewDelegate
 * A menu view delegate but segregated into multiple sections
 */
public interface SectionedMenuViewDelegate extends ViewDelegate {
    int getNumberOfSections();

    int getNumberOfOptionsForSection(int sectionNumber);

    String getLabelForSection(int sectionNumber);

    String getLabelForOption(int sectionNumber, int optionNumber);

    void onSelectOption(int sectionNumber, int optionNumber);

    default String getPromptLabel() {
        return "Select an option";
    }

    default String getExitMenuLabel() {
        return "Return to previous menu";
    }
}

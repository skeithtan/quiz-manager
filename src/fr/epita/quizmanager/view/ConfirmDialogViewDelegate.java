package fr.epita.quizmanager.view;

/**
 * ConfirmDialogViewDelegate
 * A yes-no type view.
 */
public interface ConfirmDialogViewDelegate extends ViewDelegate {
    String getPositiveLabel();

    String getNegativeLabel();

    void onSelectPositive();

    void onSelectNegative();
}

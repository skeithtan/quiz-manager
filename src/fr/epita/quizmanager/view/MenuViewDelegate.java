package fr.epita.quizmanager.view;

/**
 * MenuViewDelegate
 * A view with multiple options
 */
public interface MenuViewDelegate extends ViewDelegate {
    int getNumberOfOptions();

    String getLabelForOption(int optionNumber);

    void onSelectOption(int optionNumber);

    default String getPromptLabel() {
        return "Select an option";
    }

    default String getExitMenuLabel() {
        return "Return to previous menu";
    }
}

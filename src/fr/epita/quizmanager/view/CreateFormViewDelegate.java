package fr.epita.quizmanager.view;

/**
 * CreateFormViewDelegate
 * A form view for creating instances
 */
public interface CreateFormViewDelegate extends FormViewDelegate {
    default String getCreateReceipt() {
        return "Form submitted.";
    }
}

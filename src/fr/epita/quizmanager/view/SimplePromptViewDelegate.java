package fr.epita.quizmanager.view;

import fr.epita.quizmanager.models.ValidationResult;

/**
 * SimplePromptViewDelegate
 * A simple prompt with one field
 */
public interface SimplePromptViewDelegate extends ViewDelegate {
    ValidationResult validateResponse(String response);

    String getPromptLabel();

    void onEnterValidResponse(String response);
}

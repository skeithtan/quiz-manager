package fr.epita.quizmanager.view;

import fr.epita.quizmanager.models.ValidationResult;

/**
 * FormViewDelegate
 * A view for entering multiple custom values
 */
public interface FormViewDelegate extends ViewDelegate {
    int getNumberOfFields();

    String getFieldName(int fieldNumber);

    default String getFieldEntryInstructions(int fieldNumber) {
        return null;
    }

    ValidationResult validateValue(int fieldNumber, String value);

    void onEnterValidValue(int fieldNumber, String value);

    void onSubmitForm();

    default void onCancel() {
    }

    String getFieldValue(int fieldNumber);

    default String getFormReceipt() {
        return "Changes saved.";
    }
}

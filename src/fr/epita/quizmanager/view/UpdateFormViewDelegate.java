package fr.epita.quizmanager.view;

/**
 * UpdateFormViewDelegate
 * A form view for updating instance fields
 */
public interface UpdateFormViewDelegate extends FormViewDelegate {
    void onPreserveValue(int fieldNumber);

    String getFieldOldValue(int fieldNumber);
}

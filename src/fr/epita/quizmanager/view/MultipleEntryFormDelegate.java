package fr.epita.quizmanager.view;

import fr.epita.quizmanager.models.ValidationResult;

/**
 * MultipleEntryFormDelegate
 * A form-like view for multiple repetitive entries
 */
public interface MultipleEntryFormDelegate extends ViewDelegate {
    int getNumberOfEntries();

    String getPromptLabel(int entryNumber);

    ValidationResult validateEntry(String entry);

    void onValidEntry(String entry, int entryNumber);
}

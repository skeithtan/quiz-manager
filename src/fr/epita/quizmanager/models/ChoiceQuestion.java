package fr.epita.quizmanager.models;

import java.util.List;

public class ChoiceQuestion extends Question {
    private List<String> choices;

    private String correctAnswer;

    public ChoiceQuestion(String label, List<String> topics, int difficultyLevel, List<String> choices, String correctAnswer) {
        super(label, topics, difficultyLevel);
        this.choices = choices;
        this.correctAnswer = correctAnswer;
    }

    public ChoiceQuestion(Question question, List<String> choices, String correctAnswer) {
        super(question.getLabel(), question.getTopics(), question.getDifficultyLevel());
        this.choices = choices;
        this.correctAnswer = correctAnswer;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String choicesToString() {
        var text = new StringBuilder();

        for (int i = 0; i < choices.size(); i++) {
            text.append(String.format("\t[%d] - %s\n", i + 1, choices.get(i)));
        }

        return text.toString();
    }
}

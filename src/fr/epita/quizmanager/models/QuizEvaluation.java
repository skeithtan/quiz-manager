package fr.epita.quizmanager.models;

public class QuizEvaluation {
    private Quiz quiz;
    private Student student;
    private int multipleChoiceQuestionsCount;
    private int studentCorrectAnswersCount;

    public QuizEvaluation(Quiz quiz, Student student, int multipleChoiceQuestionsCount, int studentCorrectAnswersCount) {
        this.quiz = quiz;
        this.student = student;
        this.multipleChoiceQuestionsCount = multipleChoiceQuestionsCount;
        this.studentCorrectAnswersCount = studentCorrectAnswersCount;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getMultipleChoiceQuestionsCount() {
        return multipleChoiceQuestionsCount;
    }

    public void setMultipleChoiceQuestionsCount(int multipleChoiceQuestionsCount) {
        this.multipleChoiceQuestionsCount = multipleChoiceQuestionsCount;
    }

    public int getStudentCorrectAnswersCount() {
        return studentCorrectAnswersCount;
    }

    public void setStudentCorrectAnswersCount(int studentCorrectAnswersCount) {
        this.studentCorrectAnswersCount = studentCorrectAnswersCount;
    }
}

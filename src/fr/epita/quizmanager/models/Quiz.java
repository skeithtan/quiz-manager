package fr.epita.quizmanager.models;

import java.util.List;
import java.util.stream.Collectors;

public class Quiz {
    private String title;
    private List<Question> questions;
    private int targetDifficulty;
    private List<String> topics;

    public Quiz() {
        // For QuizBuilder
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAverageDifficulty() {
        return questions.stream()
                .mapToDouble(Question::getDifficultyLevel)
                .average()
                .orElse(0);
    }

    public int getTargetDifficulty() {
        return targetDifficulty;
    }

    public void setTargetDifficulty(int targetDifficulty) {
        this.targetDifficulty = targetDifficulty;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public List<ChoiceQuestion> getChoiceQuestions() {
        return questions.stream()
                .filter(question -> question instanceof ChoiceQuestion)
                .map(question -> (ChoiceQuestion) question)
                .collect(Collectors.toList());
    }
}

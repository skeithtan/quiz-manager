package fr.epita.quizmanager.models;

import java.util.List;

public class Question {
    public static final int MAX_DIFFICULTY = 5;
    public static final int MIN_DIFFICULTY = 1;

    private String label;
    private List<String> topics;
    private int difficultyLevel;

    public Question(String label, List<String> topics, int difficultyLevel) {
        this.label = label;
        this.topics = topics;
        this.difficultyLevel = difficultyLevel;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public int getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(int difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    @Override
    public String toString() {
        var topicsString = String.join(", ", topics);

        if (topics.size() == 0) {
            topicsString = "No assigned topics";
        }

        return String.format("Label: %s\n", label) +
                String.format("Topics (%d): %s\n", topics.size(), topicsString) +
                String.format("Difficulty level: %d", difficultyLevel);
    }
}

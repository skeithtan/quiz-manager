package fr.epita.quizmanager.models;

public class ValidationResult {
    private final boolean isValid;
    private final String validationError;
    private final static ValidationResult EMPTY_SUCCESS = new ValidationResult(true, null);

    public ValidationResult(boolean isValid, String validationError) {
        this.isValid = isValid;
        this.validationError = validationError;
    }

    public boolean isNotValid() {
        return !isValid;
    }

    public String getValidationError() {
        return validationError;
    }

    public static ValidationResult fail(String validationError) {
        return new ValidationResult(false, validationError);
    }

    public static ValidationResult success() {
        return EMPTY_SUCCESS;
    }
}

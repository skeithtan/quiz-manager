package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.MultipleEntryFormDelegate;
import fr.epita.quizmanager.view.SimplePromptViewDelegate;

import java.util.ArrayList;

public class SetQuestionChoicesPromptVD implements SimplePromptViewDelegate {
    private ArrayList<String> choices;

    @Override
    public ValidationResult validateResponse(String response) {
        int responseNumber;

        try {
            responseNumber = Integer.parseInt(response);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("Invalid number '%s'", response));
        }

        if (responseNumber < 2 || responseNumber > 6) {
            return ValidationResult.fail("Must be between 2 and 6");
        }

        return ValidationResult.success();
    }

    @Override
    public String getPromptLabel() {
        return "Number of choices (2 to 6)";
    }

    @Override
    public void onEnterValidResponse(String response) {
        var numberOfChoices = Integer.parseInt(response);
        choices = new ArrayList<>();
        RenderStack.getInstance().enterView(getChoicesEntryFormDelegate(numberOfChoices));

        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var question = Database.getPersistence().getChoiceQuestion(questionIndex);
        var newQuestion = new ChoiceQuestion(question, choices, null);
        Database.getPersistence().updateQuestion(questionIndex, newQuestion);
        RenderStack.getInstance().enterView(new SelectCorrectAnswerPromptVD() {
            @Override
            public void onExitView() {
                SetQuestionChoicesPromptVD.this.onExitView();
            }
        });
    }

    @Override
    public String getViewTitle() {
        return "Set question choices";
    }

    private MultipleEntryFormDelegate getChoicesEntryFormDelegate(int numberOfChoices) {
        return new MultipleEntryFormDelegate() {
            @Override
            public int getNumberOfEntries() {
                return numberOfChoices;
            }

            @Override
            public String getPromptLabel(int entryNumber) {
                return String.format("Choice #%d", entryNumber);
            }

            @Override
            public ValidationResult validateEntry(String entry) {
                return new ValidationResult(!entry.isBlank(), "Choice text must not be blank");
            }

            @Override
            public void onValidEntry(String entry, int entryNumber) {
                choices.add(entryNumber - 1, entry);
            }

            @Override
            public String getViewTitle() {
                return "Enter question choices";
            }
        };
    }
}

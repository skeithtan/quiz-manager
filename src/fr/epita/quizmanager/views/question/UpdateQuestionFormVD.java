package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.UpdateFormViewDelegate;

public class UpdateQuestionFormVD extends QuestionFormVD implements UpdateFormViewDelegate {
    public UpdateQuestionFormVD() {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        var question = isChoiceQuestion ?
                Database.getPersistence().getChoiceQuestion(questionIndex):
                Database.getPersistence().getOpenQuestion(questionIndex);

        this.newLabel = question.getLabel();
        this.newDifficultyLevel = question.getDifficultyLevel();
        this.newTopics = question.getTopics();
    }

    @Override
    public void onPreserveValue(int fieldNumber) {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        var question = isChoiceQuestion ?
                Database.getPersistence().getChoiceQuestion(questionIndex):
                Database.getPersistence().getOpenQuestion(questionIndex);

        switch (fieldNumber) {
            case 1:
                this.newLabel = question.getLabel();
                return;
            case 2:
                this.newDifficultyLevel = question.getDifficultyLevel();
                return;
            case 3:
                this.newTopics = question.getTopics();
        }
    }

    @Override
    public String getFieldOldValue(int fieldNumber) {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        var question = isChoiceQuestion ?
                Database.getPersistence().getChoiceQuestion(questionIndex):
                Database.getPersistence().getOpenQuestion(questionIndex);

        switch (fieldNumber) {
            case 1:
                return question.getLabel();
            case 2:
                return Integer.toString(question.getDifficultyLevel());
            case 3:
                return topicsToString(question.getTopics());
            default:
                return null;
        }
    }

    @Override
    public void onSubmitForm() {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        Question newQuestion;

        if (isChoiceQuestion) {
            var choiceQuestion = Database
                    .getPersistence()
                    .getChoiceQuestion(questionIndex);

            newQuestion = new ChoiceQuestion(
                    this.newLabel,
                    this.newTopics,
                    this.newDifficultyLevel,
                    choiceQuestion.getChoices(),
                    choiceQuestion.getCorrectAnswer());
        } else {
            newQuestion = new Question(
                    this.newLabel,
                    this.newTopics,
                    this.newDifficultyLevel
            );
        }

        Database.getPersistence().updateQuestion(questionIndex, newQuestion);
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return this.newLabel;
            case 2:
                var questionIndex = State.getInstance().getSelectedQuestionIndex();
                var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

                var question = isChoiceQuestion ?
                        Database.getPersistence().getChoiceQuestion(questionIndex):
                        Database.getPersistence().getOpenQuestion(questionIndex);

                return Integer.toString(question.getDifficultyLevel());
            case 3:
                return topicsToString(this.newTopics);
            default:
                return null;
        }
    }

    @Override
    public String getFormReceipt() {
        return "Question successfully updated";
    }

    @Override
    public String getViewTitle() {
        return "Update question";
    }

}
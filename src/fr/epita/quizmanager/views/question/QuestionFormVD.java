package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.view.FormViewDelegate;
import fr.epita.quizmanager.views.topic.TopicFormField;

import java.util.List;

public abstract class QuestionFormVD implements FormViewDelegate {
    protected String newLabel;
    protected int newDifficultyLevel;
    protected List<String> newTopics;
    private final TopicFormField topicFormField = new TopicFormField();

    @Override
    public int getNumberOfFields() {
        return 3;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return "Question label";
            case 2:
                return "Difficulty level";
            case 3:
                return "Topics";
            default:
                return null;
        }
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                return validateLabel(value);
            case 2:
                return validateDifficultyLevel(value);
            case 3:
                return topicFormField.validateTopicInputString(value);
            default:
                return null;
        }
    }

    @Override
    public String getFieldEntryInstructions(int fieldNumber) {
        switch (fieldNumber) {
            case 2:
                return getDifficultyLevelEntryInstructions();
            case 3:
                return getTopicsEntryInstructions();
            default:
                return null;
        }
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                this.newLabel = value.trim();
                return;
            case 2:
                this.newDifficultyLevel = Integer.parseInt(value);
                return;
            case 3:
                this.newTopics = topicFormField.parseTopicsInput(value);
        }
    }

    protected String topicsToString(List<String> topics) {
        return topicFormField.topicsToString(topics);
    }

    private String getDifficultyLevelEntryInstructions() {
        return String.format(
                "Select a number from %d (easiest) to %d (most difficult)",
                Question.MIN_DIFFICULTY,
                Question.MAX_DIFFICULTY
        );
    }

    private String getTopicsEntryInstructions() {
        return "Select relevant topic(s) for this question\n" + topicFormField.getTopicList();
    }

    private ValidationResult validateLabel(String value) {
        return new ValidationResult(!value.isBlank(), "The question cannot be blank.");
    }

    private ValidationResult validateDifficultyLevel(String value) {
        int level;

        try {
            level = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("%s is not a valid number", value));
        }

        if (level < Question.MIN_DIFFICULTY || level > Question.MAX_DIFFICULTY) {
            return ValidationResult.fail(String.format(
                    "Difficulty level must be a value between %d and %d",
                    Question.MIN_DIFFICULTY,
                    Question.MAX_DIFFICULTY
            ));
        }

        return ValidationResult.success();
    }
}

package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.ConfirmDialogViewDelegate;
import fr.epita.quizmanager.view.MenuViewDelegate;

public class QuestionActionsMenuVD implements MenuViewDelegate {
    @Override
    public String getViewText() {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();

        var question = State.getInstance().selectedQuestionHasChoices() ?
                Database.getPersistence().getChoiceQuestion(questionIndex):
                Database.getPersistence().getOpenQuestion(questionIndex);

        return question.toString();
    }

    @Override
    public int getNumberOfOptions() {
        return 3;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        switch (optionNumber) {
            case 1:
                return "Update question details";
            case 2:
                return "Delete question";
            case 3:
                return isChoiceQuestion ? "Manage question choices" : "Set question choices";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                var delegate = new UpdateQuestionFormVD();
                RenderStack.getInstance().enterView(delegate);
                return;
            case 2:
                RenderStack.getInstance().enterView(getConfirmDeleteDelegate());
                return;
            case 3:
                onSetQuestionChoices();
                return;
            case 4:

        }
    }

    @Override
    public String getViewTitle() {
        return "View Question";
    }

    private void onSetQuestionChoices() {
        var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

        if (isChoiceQuestion) {
            RenderStack.getInstance().enterView(new ManageQuestionChoicesMenuVD());
        } else {
            RenderStack.getInstance().enterView(getConfirmTransformToChoiceQuestionDelegate());
        }
    }

    private ConfirmDialogViewDelegate getConfirmDeleteDelegate() {
        return new ConfirmDialogViewDelegate() {
            @Override
            public String getPositiveLabel() {
                return "Confirm delete question";
            }

            @Override
            public String getNegativeLabel() {
                return "Cancel";
            }

            @Override
            public void onSelectPositive() {
                var questionIndex = State.getInstance().getSelectedQuestionIndex();
                var isChoiceQuestion = State.getInstance().selectedQuestionHasChoices();

                if (isChoiceQuestion) {
                    Database.getPersistence().removeChoiceQuestion(questionIndex);
                } else {
                    Database.getPersistence().removeOpenQuestion(questionIndex);
                }

                QuestionActionsMenuVD.this.onExitView();
            }

            @Override
            public void onSelectNegative() {
                RenderStack.getInstance().exitToParentAndRenderTop(QuestionActionsMenuVD.this);
            }

            @Override
            public String getViewTitle() {
                return null;
            }
        };
    }

    private ConfirmDialogViewDelegate getConfirmTransformToChoiceQuestionDelegate() {
        return new ConfirmDialogViewDelegate() {
            @Override
            public String getPositiveLabel() {
                return "Confirm transform open question to multiple choice question";
            }

            @Override
            public String getNegativeLabel() {
                return "Cancel";
            }

            @Override
            public void onSelectPositive() {
                onTransformToChoiceQuestion();
            }

            @Override
            public void onSelectNegative() {
                this.onExitView();
            }

            @Override
            public String getViewTitle() {
                return "Transform to Multiple Choice Question";
            }

            private void onTransformToChoiceQuestion() {
                var state = State.getInstance();
                var database = Database.getPersistence();
                var questionIndex = state.getSelectedQuestionIndex();
                var question = database.getOpenQuestion(questionIndex);
                var choiceQuestion = new ChoiceQuestion(question, null, null);

                database.removeOpenQuestion(question);
                var choiceQuestionIndex = database.addChoiceQuestion(choiceQuestion);
                state.setSelectedQuestion(choiceQuestionIndex, true);

                RenderStack.getInstance().enterView(new SetQuestionChoicesPromptVD() {
                    @Override
                    public void onExitView() {
                        exitView();
                    }
                });
            }

            private void exitView() {
                this.onExitView();
            }
        };
    }
}

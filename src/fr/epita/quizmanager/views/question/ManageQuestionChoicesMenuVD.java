package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.ConfirmDialogViewDelegate;
import fr.epita.quizmanager.view.MenuViewDelegate;

public class ManageQuestionChoicesMenuVD implements MenuViewDelegate {
    @Override
    public int getNumberOfOptions() {
        return 3;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                return "Replace correct answer";
            case 2:
                return "Replace all choices";
            case 3:
                return "Transform to open question";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                RenderStack.getInstance().enterView(new SelectCorrectAnswerPromptVD());
                return;
            case 2:
                RenderStack.getInstance().enterView(new SetQuestionChoicesPromptVD());
                return;
            case 3:
                RenderStack.getInstance().enterView(getConfirmTransformToOpenQuestionDelegate());
        }
    }

    @Override
    public String getViewTitle() {
        return "Manage question choices";
    }

    @Override
    public String getViewText() {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var question = Database.getPersistence().getChoiceQuestion(questionIndex);
        return String.format("%s\nCorrect answer: '%s'\n", question.choicesToString(), question.getCorrectAnswer());
    }

    private void onTransformToOpenQuestion() {
        var state = State.getInstance();
        var database = Database.getPersistence();
        var questionIndex = state.getSelectedQuestionIndex();
        var question = database.getChoiceQuestion(questionIndex);
        Database.getPersistence().removeChoiceQuestion(question);

        var newQuestion = new Question(question.getLabel(), question.getTopics(), question.getDifficultyLevel());
        var openQuestionIndex = database.addOpenQuestion(newQuestion);
        state.setSelectedQuestion(openQuestionIndex, false);

        this.onExitView();
    }

    private ConfirmDialogViewDelegate getConfirmTransformToOpenQuestionDelegate() {
        return new ConfirmDialogViewDelegate() {
            @Override
            public String getPositiveLabel() {
                return "Confirm remove choices and transform to open questions";
            }

            @Override
            public String getNegativeLabel() {
                return "Cancel";
            }

            @Override
            public void onSelectPositive() {
                onTransformToOpenQuestion();
            }

            @Override
            public void onSelectNegative() {
                this.onExitView();
            }

            @Override
            public String getViewTitle() {
                return "Transform to open question";
            }
        };
    }
}

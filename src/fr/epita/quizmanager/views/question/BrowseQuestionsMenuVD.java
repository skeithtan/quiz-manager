package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.SectionedMenuViewDelegate;

import java.util.ArrayList;
import java.util.List;

public class BrowseQuestionsMenuVD implements SectionedMenuViewDelegate {

    private static class QuestionIndex<Q> {
        final int index;
        final Q question;

        private QuestionIndex(int index, Q question) {
            this.index = index;
            this.question = question;
        }
    }

    @Override
    public int getNumberOfSections() {
        return 2;
    }

    @Override
    public int getNumberOfOptionsForSection(int sectionNumber) {
        switch (sectionNumber) {
            case 1:
                return getFilteredOpenQuestions().size();
            case 2:
                return getFilteredChoiceQuestions().size();
            default:
                return 0;
        }
    }

    @Override
    public String getLabelForSection(int sectionNumber) {
        switch (sectionNumber) {
            case 1:
                return String.format("Open questions (%d)", getFilteredOpenQuestions().size());
            case 2:
                return String.format("Choice questions (%d)", getFilteredChoiceQuestions().size());
            default:
                return null;
        }
    }

    @Override
    public String getLabelForOption(int sectionNumber, int optionNumber) {
        var index = optionNumber - 1;
        var questionIndex = sectionNumber == 1 ?
                getFilteredOpenQuestions().get(index):
                getFilteredChoiceQuestions().get(index);

        return questionIndex.question.getLabel();
    }

    @Override
    public void onSelectOption(int sectionNumber, int optionNumber) {
        var optionIndex = optionNumber - 1;
        var isChoiceQuestion = sectionNumber == 2;
        var state = State.getInstance();

        var questionIndex = isChoiceQuestion ?
                getFilteredChoiceQuestions().get(optionIndex) :
                getFilteredOpenQuestions().get(optionIndex);

        state.setSelectedQuestion(questionIndex.index, isChoiceQuestion);

        var delegate = new QuestionActionsMenuVD();
        RenderStack.getInstance().enterView(delegate);
    }

    @Override
    public String getPromptLabel() {
        return "Select a question";
    }

    @Override
    public String getViewTitle() {
        var questionCount = getFilteredOpenQuestions().size() + getFilteredChoiceQuestions().size();
        return String.format("Questions on '%s' (%d)", this.getParentTopic(), questionCount);
    }

    private String getParentTopic() {
        return Database.getPersistence().getTopic(
                State.getInstance().getSelectedTopicIndex()
        );
    }

    private List<QuestionIndex<Question>> getFilteredOpenQuestions() {
        var openQuestions = Database.getPersistence().getOpenQuestions();
        var list = new ArrayList<QuestionIndex<Question>>();

        for (int index = 0; index < openQuestions.length; index++) {
            if (openQuestions[index].getTopics().contains(getParentTopic())) {
                list.add(new QuestionIndex<>(index, openQuestions[index]));
            }
        }
        return list;
    }

    private List<QuestionIndex<ChoiceQuestion>> getFilteredChoiceQuestions() {
        var choiceQuestions = Database.getPersistence().getChoiceQuestions();
        var list = new ArrayList<QuestionIndex<ChoiceQuestion>>();

        for (int index = 0; index < choiceQuestions.length; index++) {
            if (choiceQuestions[index].getTopics().contains(getParentTopic())) {
                list.add(new QuestionIndex<>(index, choiceQuestions[index]));
            }
        }

        return list;
    }

}

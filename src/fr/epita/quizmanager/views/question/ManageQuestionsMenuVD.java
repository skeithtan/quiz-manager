package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.MenuViewDelegate;
import fr.epita.quizmanager.views.topic.TopicListMenuVD;

public class ManageQuestionsMenuVD implements MenuViewDelegate {
    @Override
    public int getNumberOfOptions() {
        return 2;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                return "Browse questions by topic";
            case 2:
                return "Create a new question";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        var renderStack = RenderStack.getInstance();

        switch (optionNumber) {
            case 1:
                renderStack.enterView(getBrowseTopicVC());
                return;
            case 2:
                renderStack.enterView(new CreateQuestionFormVD());
        }
    }

    @Override
    public String getViewTitle() {
        return "Manage Questions";
    }

    public MenuViewDelegate getBrowseTopicVC() {
        return new TopicListMenuVD() {
            @Override
            public void onSelectTopic(String selectedTopic, int index) {
                State.getInstance().setSelectedTopicIndex(index);
                RenderStack.getInstance().enterView(new BrowseQuestionsMenuVD());
            }

            @Override
            public String getViewTitle() {
                return "Select a Topic";
            }
        };
    }
}

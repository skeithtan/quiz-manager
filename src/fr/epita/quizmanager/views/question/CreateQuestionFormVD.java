package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.CreateFormViewDelegate;

public class CreateQuestionFormVD extends QuestionFormVD implements CreateFormViewDelegate {
    private boolean isChoiceQuestion;
    private boolean didCreateQuestion = false;

    @Override
    public String getViewTitle() {
        return "Create a question";
    }

    @Override
    public void onSubmitForm() {
        var question = new Question(newLabel, newTopics, newDifficultyLevel);
        int index;

        if (isChoiceQuestion) {
            index = Database.getPersistence()
                    .addChoiceQuestion(new ChoiceQuestion(question, null, null));
        } else {
            index = Database.getPersistence()
                    .addOpenQuestion(question);
        }

        didCreateQuestion = true;
        State.getInstance().setSelectedQuestion(index, isChoiceQuestion);
    }

    @Override
    public void onExitView() {
        if (this.isChoiceQuestion && this.didCreateQuestion) {
            RenderStack.getInstance().exitToParent(this);
            RenderStack.getInstance().enterView(new SetQuestionChoicesPromptVD());
        } else {
            super.onExitView();
        }
    }

    @Override
    public int getNumberOfFields() {
        return 4;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
            case 2:
            case 3:
                return super.getFieldName(fieldNumber);
            case 4:
                return "Does this question have choices? (Y/N)";
            default:
                return null;
        }
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
            case 2:
            case 3:
                return super.validateValue(fieldNumber, value);
            case 4:
                value = value.toUpperCase();
                return new ValidationResult(
                        value.equals("Y") || value.equals("N"),
                        String.format("Invalid entry %s", value)
                );
            default:
                return null;
        }
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
            case 2:
            case 3:
                super.onEnterValidValue(fieldNumber, value);
                return;
            case 4:
                this.isChoiceQuestion = value.toUpperCase().equals("Y");
        }
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return this.newLabel;
            case 2:
                return Integer.toString(this.newDifficultyLevel);
            case 3:
                return topicsToString(this.newTopics);
            default:
                return this.isChoiceQuestion ? "Yes" : "No";
        }
    }

    @Override
    public String getCreateReceipt() {
        return "Question successfully created.";
    }
}

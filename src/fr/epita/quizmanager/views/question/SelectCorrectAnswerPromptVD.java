package fr.epita.quizmanager.views.question;

import fr.epita.quizmanager.models.ChoiceQuestion;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.SimplePromptViewDelegate;

public class SelectCorrectAnswerPromptVD implements SimplePromptViewDelegate {
    @Override
    public ValidationResult validateResponse(String response) {
        int responseNumber;

        try {
            responseNumber = Integer.parseInt(response);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("Invalid selection '%s'", response));
        }

        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var question = Database.getPersistence().getChoiceQuestion(questionIndex);
        var numberOfChoices = question.getChoices().size();
        if (responseNumber < 1 || responseNumber > numberOfChoices) {
            return ValidationResult.fail(String.format("Must be between 1 and %d.", numberOfChoices));
        }

        return ValidationResult.success();
    }

    @Override
    public String getPromptLabel() {
        return "New correct answer";
    }

    @Override
    public void onEnterValidResponse(String response) {
        onReplaceCorrectAnswer(Integer.parseInt(response));
    }

    @Override
    public String getViewTitle() {
        return "Select new correct answer";
    }

    @Override
    public String getViewText() {
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var question = Database.getPersistence().getChoiceQuestion(questionIndex);

        return String.format(
                "Question choices: \n%s\nEnter the number of the new correct answer",
                question.choicesToString()
        );
    }

    private void onReplaceCorrectAnswer(int choiceNumber) {
        var choiceIndex = choiceNumber - 1;
        var questionIndex = State.getInstance().getSelectedQuestionIndex();
        var question = Database.getPersistence().getChoiceQuestion(questionIndex);

        var correctAnswer = question.getChoices().get(choiceIndex);

        if (correctAnswer.equals(question.getCorrectAnswer())) {
            return;
        }

        var newChoiceQuestion = new ChoiceQuestion(question, question.getChoices(), correctAnswer);
        Database.getPersistence().updateQuestion(questionIndex, newChoiceQuestion);
        this.onExitView();
    }
}

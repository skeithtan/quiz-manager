package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.view.MenuViewDelegate;

public class ManageTopicsMenuVD implements MenuViewDelegate {
    @Override
    public String getViewTitle() {
        return "Manage Topics";
    }

    @Override
    public int getNumberOfOptions() {
        return 2;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                var topicCount = Database.getPersistence().getTopics().length;
                return String.format("Browse existing topics (%d in database)", topicCount);
            case 2:
                return "Create a new topic";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                RenderStack.getInstance().enterView(new BrowseTopicMenuVD());
                return;
            case 2:
                RenderStack.getInstance().enterView(new CreateTopicFormVD());
        }
    }
}

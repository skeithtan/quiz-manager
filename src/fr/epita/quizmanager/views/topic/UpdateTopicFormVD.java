package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.view.UpdateFormViewDelegate;

public class UpdateTopicFormVD implements UpdateFormViewDelegate {
    private final String selectedTopic;
    private final int topicIndex;

    private String newName;

    public UpdateTopicFormVD(String selectedTopic, int topicIndex) {
        this.selectedTopic = selectedTopic;
        this.topicIndex = topicIndex;
        this.newName = selectedTopic;
    }

    @Override
    public String getViewTitle() {
        return String.format("Update topic: '%s'", selectedTopic);
    }

    @Override
    public int getNumberOfFields() {
        return 1;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        return "Topic name";
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        return new ValidationResult(!value.isBlank(), "Topic name cannot be blank.");
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        this.newName = value;
    }

    @Override
    public void onPreserveValue(int fieldNumber) {
        newName = selectedTopic;
    }

    @Override
    public void onSubmitForm() {
        Database.getPersistence().updateTopic(topicIndex, this.newName);
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        return this.newName;
    }

    @Override
    public String getFormReceipt() {
        return String.format("Topic %s successfully updated.", selectedTopic);
    }

    @Override
    public String getFieldOldValue(int fieldNumber) {
        return this.selectedTopic;
    }
}

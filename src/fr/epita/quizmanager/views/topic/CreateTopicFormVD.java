package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.view.CreateFormViewDelegate;

public class CreateTopicFormVD implements CreateFormViewDelegate {
    private String topicName;

    @Override
    public String getViewTitle() {
        return "Create a topic";
    }

    @Override
    public int getNumberOfFields() {
        return 1;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        return "Topic name";
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        return new ValidationResult(!value.isBlank(), "Topic name cannot be blank.");
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        this.topicName = value;
    }

    @Override
    public void onSubmitForm() {
        Database.getPersistence().addTopic(this.topicName);
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        return this.topicName;
    }

    @Override
    public String getCreateReceipt() {
        return String.format("Topic %s created", this.topicName);
    }
}

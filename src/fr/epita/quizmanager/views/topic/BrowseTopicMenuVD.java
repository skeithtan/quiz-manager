package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.services.renderer.RenderStack;

public class BrowseTopicMenuVD extends TopicListMenuVD {
    @Override
    public String getViewTitle() {
        return "All Topics";
    }

    @Override
    public void onSelectTopic(String selectedTopic, int index) {
        RenderStack.getInstance().enterView(new UpdateTopicFormVD(selectedTopic, index));
    }

    @Override
    public String getPromptLabel() {
        return "Select topic to update";
    }
}


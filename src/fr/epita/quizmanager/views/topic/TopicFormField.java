package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.database.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TopicFormField {
    public ValidationResult validateTopicInputString(String value) {
        var topics = Database.getPersistence().getTopics();
        var inputs = value.split(" ");

        for (var token : inputs) {
            int topicNumber;
            try {
                topicNumber = Integer.parseInt(token);
            } catch (NumberFormatException e) {
                return ValidationResult.fail(String.format("Invalid entry '%s'.", token));
            }

            if (topicNumber < 1 || topicNumber > topics.length) {
                return ValidationResult.fail(String.format("Invalid entry '%s'.", token));
            }

            // NOTE: Duplicate entries will just be filtered out, but will be considered valid
        }

        return ValidationResult.success();
    }

    public String getTopicList() {
        StringBuilder topicInstructions = new StringBuilder();
        var topics = Database.getPersistence().getTopics();

        for (int i = 0; i < topics.length; i++) {
            topicInstructions.append(String.format("\t[%d] - %s\n", i + 1, topics[i]));
        }

        topicInstructions.append("\nEnter the number of applicable topics separated by a space");
        return topicInstructions.toString();
    }

    public String topicsToString(List<String> topics) {
        return String.join(", ", topics);
    }

    public List<String> parseTopicsInput(String input) {
        var topics = Database.getPersistence().getTopics();
        var inputs = input.split(" ");
        var topicIndices = new ArrayList<Integer>();

        for (var token : inputs) {
            int topicIndex = Integer.parseInt(token) - 1;

            if (!topicIndices.contains(topicIndex)) {
                topicIndices.add(topicIndex);
            }
        }

        return topicIndices.stream().map(index -> topics[index]).collect(Collectors.toList());
    }
}

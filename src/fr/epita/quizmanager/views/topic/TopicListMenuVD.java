package fr.epita.quizmanager.views.topic;

import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.view.MenuViewDelegate;

public abstract class TopicListMenuVD implements MenuViewDelegate {
    @Override
    public int getNumberOfOptions() {
        return Database.getPersistence().getTopics().length;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        var topics = Database.getPersistence().getTopics();
        return topics[optionNumber - 1];
    }

    @Override
    public void onSelectOption(int optionNumber) {
        var index = optionNumber - 1;
        var selectedTopic = Database.getPersistence().getTopics()[index];
        onSelectTopic(selectedTopic, index);
    }

    public abstract void onSelectTopic(String selectedTopic, int index);
}

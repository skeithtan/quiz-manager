package fr.epita.quizmanager.views.quiz;

import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.MenuViewDelegate;
import fr.epita.quizmanager.views.evaluation.ManageEvaluationsMenuVD;

public class QuizMenuVD implements MenuViewDelegate {
    @Override
    public int getNumberOfOptions() {
        return 3;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                return "Evaluate student answers";
            case 2:
                return "Save quiz as text file";
            case 3:
                return "Discard quiz and create a new quiz";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                onSelectManageEvaluations();
                return;
            case 2:
                onSelectSaveQuiz();
                return;
            case 3:
                onSelectDiscardQuiz();
        }
    }

    @Override
    public String getViewTitle() {
        return String.format("Manage quiz '%s'", State.getInstance().getCurrentQuiz().getTitle());
    }

    @Override
    public String getViewText() {
        var quiz = State.getInstance().getCurrentQuiz();
        return String.format("\tNumber of questions: %d\n", quiz.getQuestions().size()) +
                String.format("\tAverage difficulty: %s\n", quiz.getAverageDifficulty());
    }

    private void onSelectManageEvaluations() {
        RenderStack.getInstance().enterView(new ManageEvaluationsMenuVD());
    }

    private void onSelectSaveQuiz() {
        RenderStack.getInstance().enterView(new ConfirmSaveQuizVD());
    }

    private void onSelectDiscardQuiz() {
        var renderStack = RenderStack.getInstance();
        renderStack.exitToParent(this);
        renderStack.enterView(new CreateQuizFormVD());
    }
}

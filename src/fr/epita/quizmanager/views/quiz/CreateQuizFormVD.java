package fr.epita.quizmanager.views.quiz;

import fr.epita.quizmanager.models.Question;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.quiz.QuizBuilder;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.CreateFormViewDelegate;
import fr.epita.quizmanager.views.topic.TopicFormField;

public class CreateQuizFormVD implements CreateFormViewDelegate {
    private final QuizBuilder quizBuilder = new QuizBuilder();
    private final TopicFormField topicFormField = new TopicFormField();

    @Override
    public int getNumberOfFields() {
        return 4;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return "Quiz title";
            case 2:
                return "Quiz topics";
            case 3:
                return "Target difficulty";
            case 4:
                return "Target number of questions";
            default:
                return null;
        }
    }

    @Override
    public String getFieldEntryInstructions(int fieldNumber) {
        switch (fieldNumber) {
            case 2:
                return "Select relevant topic(s) for this quiz\n" + topicFormField.getTopicList();
            case 3:
                return String.format(
                        "Select a number from %d (easiest) to %d (most difficult)",
                        Question.MIN_DIFFICULTY,
                        Question.MAX_DIFFICULTY
                );
            default:
                return null;
        }
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                return new ValidationResult(!value.isBlank(), "Quiz title must not be blank");
            case 2:
                return topicFormField.validateTopicInputString(value);
            case 3:
                return validateDifficultyLevel(value);
            case 4:
                return validateNumberOfQuestions(value);
            default:
                return null;
        }
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                quizBuilder.setTitle(value);
                return;
            case 2:
                quizBuilder.setTopics(topicFormField.parseTopicsInput(value));
                return;
            case 3:
                quizBuilder.setTargetDifficulty(Integer.parseInt(value));
                return;
            case 4:
                quizBuilder.setTargetQuestionsCount(Integer.parseInt(value));
        }
    }

    @Override
    public void onSubmitForm() {
        var quiz = quizBuilder.build();
        State.getInstance().setCurrentQuiz(quiz);
    }

    @Override
    public void onExitView() {
        if (State.getInstance().getCurrentQuiz() == null) {
            CreateFormViewDelegate.super.onExitView();
        } else {
            RenderStack.getInstance().exitToParent(this);
            RenderStack.getInstance().enterView(new QuizMenuVD());
        }
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return quizBuilder.getTitle();
            case 2:
                return topicFormField.topicsToString(quizBuilder.getTopics());
            case 3:
                return Integer.toString(quizBuilder.getTargetDifficulty());
            case 4:
                return Integer.toString(quizBuilder.getTargetQuestionsCount());
            default:
                return null;
        }
    }

    @Override
    public String getViewTitle() {
        return "Create a quiz";
    }

    private ValidationResult validateDifficultyLevel(String value) {
        int level;

        try {
            level = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("%s is not a valid number", value));
        }

        if (level < Question.MIN_DIFFICULTY || level > Question.MAX_DIFFICULTY) {
            return ValidationResult.fail(String.format(
                    "Difficulty level must be a value between %d and %d",
                    Question.MIN_DIFFICULTY,
                    Question.MAX_DIFFICULTY
            ));
        }

        return ValidationResult.success();
    }

    private ValidationResult validateNumberOfQuestions(String value) {
        int numberOfQuestions;

        try {
            numberOfQuestions = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("%s is not a valid number", value));
        }

        var allQuestionsLength = quizBuilder.findAllTopicQuestions().size();
        if (numberOfQuestions > allQuestionsLength) {
            return ValidationResult.fail(String.format(
                    "There are not enough questions in the database for the selected topics (Max questions %d)",
                    allQuestionsLength
            ));
        }

        return new ValidationResult(
                numberOfQuestions > 0,
                "Number of questions must be greater than 0"
        );
    }

    @Override
    public String getFormReceipt() {
        var quiz = State.getInstance().getCurrentQuiz();
        return String.format("Quiz created with closest average difficulty %s", quiz.getAverageDifficulty());
    }
}

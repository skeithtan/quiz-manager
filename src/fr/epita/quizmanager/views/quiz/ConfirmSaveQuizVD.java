package fr.epita.quizmanager.views.quiz;

import fr.epita.quizmanager.services.quiz.QuizSerializer;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.ConfirmDialogViewDelegate;

public class ConfirmSaveQuizVD implements ConfirmDialogViewDelegate {
    @Override
    public String getPositiveLabel() {
        return "Save and overwrite existing file (if any)";
    }

    @Override
    public String getNegativeLabel() {
        return "Cancel";
    }

    @Override
    public void onSelectPositive() {
        var quiz = State.getInstance().getCurrentQuiz();
        var quizFileName = String.format("%s.txt", quiz.getTitle());
        new QuizSerializer(quiz).makeFile(quizFileName);
        System.out.printf("Quiz successfully saved as '%s'\n", quizFileName);
    }

    @Override
    public void onSelectNegative() {
        this.onExitView();
    }

    @Override
    public String getViewTitle() {
        return String.format("Confirm save quiz %s", State.getInstance().getCurrentQuiz().getTitle());
    }
}

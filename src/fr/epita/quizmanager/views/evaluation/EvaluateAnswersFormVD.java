package fr.epita.quizmanager.views.evaluation;

import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.CreateFormViewDelegate;

import java.util.HashMap;
import java.util.Map;

public class EvaluateAnswersFormVD implements CreateFormViewDelegate {
    private final Map<Integer, String> studentAnswers = new HashMap<>();

    @Override
    public int getNumberOfFields() {
        return State.getInstance().getCurrentQuiz().getChoiceQuestions().size();
    }

    @Override
    public String getFieldName(int fieldNumber) {
        return String.format("Answer to question #%d", fieldNumber);
    }

    @Override
    public String getFieldEntryInstructions(int fieldNumber) {
        var question = State.getInstance()
                .getCurrentQuiz()
                .getChoiceQuestions()
                .get(fieldNumber - 1);

        var choices = question.choicesToString();

        return String.format("%d.) %s\n%s", fieldNumber, question.getLabel(), choices);
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        var question = State.getInstance()
                .getCurrentQuiz()
                .getChoiceQuestions()
                .get(fieldNumber - 1);

        var choiceCount = question.getChoices().size();

        int responseNumber;

        try {
            responseNumber = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("Invalid option '%s' selected", value));
        }

        if (responseNumber <= 0 || responseNumber > choiceCount) {
            return ValidationResult.fail(String.format("Invalid option '%s' selected", value));
        }

        return ValidationResult.success();
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        var choiceIndex = Integer.parseInt(value) - 1;
        var questionIndex = fieldNumber - 1;
        var choice = State
                .getInstance()
                .getCurrentQuiz()
                .getChoiceQuestions()
                .get(questionIndex)
                .getChoices()
                .get(choiceIndex);

        studentAnswers.put(questionIndex, choice);
    }

    @Override
    public void onSubmitForm() {
        var state = State.getInstance();
        var quizLength = state.getCurrentQuiz().getChoiceQuestions().size();

        var correctAnswers = 0;

        for (int i = 0; i < quizLength; i++) {
            var question = state.getCurrentQuiz().getChoiceQuestions().get(i);
            var studentAnswer = studentAnswers.get(i);
            if (question.getCorrectAnswer().equals(studentAnswer)) {
                correctAnswers++;
            }
        }

        state.getSelectedEvaluation().setStudentCorrectAnswersCount(correctAnswers);
    }

    @Override
    public String getFormReceipt() {
        var evaluation = State.getInstance().getSelectedEvaluation();

        return String.format(
                "Student '%s' got %d items correct over %d total multiple choice questions",
                evaluation.getStudent().getName(),
                evaluation.getStudentCorrectAnswersCount(),
                evaluation.getMultipleChoiceQuestionsCount()
        );
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        return studentAnswers.get(fieldNumber - 1);
    }

    @Override
    public String getViewTitle() {
        return "Evaluate multiple choice answers";
    }

    @Override
    public void onCancel() {
        State.getInstance().removeCurrentEvaluation();
        CreateFormViewDelegate.super.onCancel();
    }
}

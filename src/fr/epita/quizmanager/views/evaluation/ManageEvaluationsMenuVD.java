package fr.epita.quizmanager.views.evaluation;

import fr.epita.quizmanager.models.QuizEvaluation;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.MenuViewDelegate;

public class ManageEvaluationsMenuVD implements MenuViewDelegate {
    @Override
    public int getNumberOfOptions() {
        return State.getInstance().getCurrentQuiz().getChoiceQuestions().size() > 0 ? 1 : 0;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        return "Create new evaluation";
    }

    @Override
    public void onSelectOption(int optionNumber) {
        RenderStack.getInstance().enterView(new EvaluateStudentFormVD());
    }

    @Override
    public String getViewTitle() {
        return "Student evaluations";
    }

    @Override
    public String getViewText() {
        var questions = State.getInstance().getCurrentQuiz().getChoiceQuestions();
        var evaluations = State.getInstance().getEvaluations();

        if (questions.size() == 0) {
            return "Cannot evaluate a quiz without multiple choice questions.\n";
        }

        if (evaluations.length == 0) {
            return "No evaluations yet. When an evaluation is created, they will be listed here.\n";
        }

        return evaluationsToString(evaluations);
    }

    private String evaluationsToString(QuizEvaluation[] evaluations) {
        var quiz = State.getInstance().getCurrentQuiz();
        var output = new StringBuilder(String.format("\tTotal multiple choice questions: %d\n", quiz.getChoiceQuestions().size()));
        output.append("\tStudent ID / Student Name / Correct items (percentage)\n");

        for (var evaluation : evaluations) {
            output.append(evaluationToString(evaluation));
        }

        return output.toString();
    }

    private String evaluationToString(QuizEvaluation evaluation) {
        var correctItems = evaluation.getStudentCorrectAnswersCount();
        var percentage = correctItems / (double) evaluation.getMultipleChoiceQuestionsCount();
        percentage *= 100;

        return String.format(
                "\t%d / %s / %d items correct (%s%%)\n",
                evaluation.getStudent().getId(),
                evaluation.getStudent().getName(),
                correctItems,
                percentage
        );
    }
}

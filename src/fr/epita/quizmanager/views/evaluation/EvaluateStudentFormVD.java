package fr.epita.quizmanager.views.evaluation;

import fr.epita.quizmanager.models.QuizEvaluation;
import fr.epita.quizmanager.models.Student;
import fr.epita.quizmanager.models.ValidationResult;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.CreateFormViewDelegate;

public class EvaluateStudentFormVD implements CreateFormViewDelegate {
    private int studentId;
    private String studentName;
    private boolean didCreateEvaluation = false;

    @Override
    public int getNumberOfFields() {
        return 2;
    }

    @Override
    public String getFieldName(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return "Student ID";
            case 2:
                return "Name";
            default:
                return null;
        }
    }

    @Override
    public ValidationResult validateValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                return validateStudentId(value);
            case 2:
                return new ValidationResult(!value.isBlank(), "Name cannot be blank");
            default:
                return null;
        }
    }

    @Override
    public void onEnterValidValue(int fieldNumber, String value) {
        switch (fieldNumber) {
            case 1:
                this.studentId = Integer.parseInt(value);
                return;
            case 2:
                this.studentName = value.strip();
        }
    }

    @Override
    public void onSubmitForm() {
        var state = State.getInstance();
        var student = new Student(studentId, studentName);
        var quiz = state.getCurrentQuiz();
        var evaluation = new QuizEvaluation(quiz, student, quiz.getChoiceQuestions().size(), 0);
        state.addEvaluationAndSetCurrent(evaluation);
        didCreateEvaluation = true;
    }

    @Override
    public String getFieldValue(int fieldNumber) {
        switch (fieldNumber) {
            case 1:
                return Integer.toString(this.studentId);
            case 2:
                return this.studentName;
            default:
                return null;
        }
    }

    @Override
    public String getViewTitle() {
        return "Evaluate student answers: Student details";
    }

    private ValidationResult validateStudentId(String value) {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return ValidationResult.fail(String.format("Invalid number '%s'", value));
        }

        return ValidationResult.success();
    }

    @Override
    public void onExitView() {
        if (didCreateEvaluation) {
            var renderStack = RenderStack.getInstance();
            renderStack.exitToParent(this);
            renderStack.enterView(new EvaluateAnswersFormVD());
        } else {
            CreateFormViewDelegate.super.onExitView();
        }
    }


}

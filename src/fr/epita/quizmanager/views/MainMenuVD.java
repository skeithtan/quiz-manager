package fr.epita.quizmanager.views;

import fr.epita.quizmanager.services.database.Database;
import fr.epita.quizmanager.services.renderer.RenderStack;
import fr.epita.quizmanager.services.state.State;
import fr.epita.quizmanager.view.MenuViewDelegate;
import fr.epita.quizmanager.views.question.ManageQuestionsMenuVD;
import fr.epita.quizmanager.views.quiz.CreateQuizFormVD;
import fr.epita.quizmanager.views.quiz.QuizMenuVD;
import fr.epita.quizmanager.views.topic.ManageTopicsMenuVD;

public class MainMenuVD implements MenuViewDelegate {
    @Override
    public String getViewTitle() {
        return "Quiz Manager by Keith Tan";
    }

    @Override
    public int getNumberOfOptions() {
        if (dbHasNoTopics()) {
            return 1;
        }

        if (dbHasNoQuestions()) {
            return 2;
        }

        return 3;
    }

    @Override
    public String getLabelForOption(int optionNumber) {
        switch (optionNumber) {
            case 1:
                return "Manage Topics";
            case 2:
                return "Manage Questions";
            case 3:
                return "Manage Quiz";
            default:
                return null;
        }
    }

    @Override
    public void onSelectOption(int optionNumber) {
        var renderStack = RenderStack.getInstance();

        switch (optionNumber) {
            case 1:
                renderStack.enterView(new ManageTopicsMenuVD());
                return;
            case 2:
                renderStack.enterView(new ManageQuestionsMenuVD());
                return;
            case 3:
                onSelectManageQuiz();
        }
    }

    @Override
    public String getExitMenuLabel() {
        return "Exit Quiz Manager";
    }

    @Override
    public String getViewText() {
        if (dbHasNoTopics()) {
            return "No topics found in the database. Add some first to be able to manage questions.";
        }

        if (dbHasNoQuestions()) {
            return "No questions found in the database. Add some first to be able to make quizzes.";
        }

        return null;
    }

    private boolean dbHasNoTopics() {
        return Database.getPersistence().getTopics().length == 0;
    }

    private boolean dbHasNoQuestions() {
        return Database.getPersistence().getAllQuestions().length == 0;
    }

    private void onSelectManageQuiz() {
        var state = State.getInstance();
        var renderStack = RenderStack.getInstance();

        if (state.getCurrentQuiz() == null) {
            renderStack.enterView(new CreateQuizFormVD());
        } else {
            renderStack.enterView(new QuizMenuVD());
        }
    }
}
